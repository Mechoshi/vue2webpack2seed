# README #


### What is this repository for? ###

* VueJS 2 seed with TypeScript 2 and Webpack 2
* Version: 1.0.0
* [VueJS](https://vuejs.org/)
* [TypeScript](https://www.typescriptlang.org/)
* [Webpack 2](https://webpack.js.org/)
* [vue-class-component](https://github.com/vuejs/vue-class-component) - use class decorators in .ts files

### Workflow: ###

Recomended is the [git flow](https://danielkummer.github.io/git-flow-cheatsheet/) workflow with default settings.



### How do I get set up? ###

1. Clone the repo in a local folder on your machine:

    a) Open console and navigate to your projects directory

    b) Execute:

    ```
    git clone https://Mechoshi@bitbucket.org/Mechoshi/vue2webpack2seed.git
    ```
    NB: That wil create new folder named 'vue2webpack2seed' and copy the code inside.
        To copy in a directory without creating tis directory, navigate to the 
        folder you want to use and execute:

    
        git clone https://Mechoshi@bitbucket.org/Mechoshi/vue2webpack2seed.git .

 2. In the created directory, in the console execute:       

    ```
    npm install
    ```

    NB: You need [nodeJS](https://nodejs.org/en/) witn npm installed!

### Run in develop mode ###

1. In the root directory of the project, in console execute:

    ```
    npm run develop
    ```
2. Open your browser and go to [localhost:8080](http://localhost:8080)

### Run for production ###

1. In the root directory of the project, in console execute:

    ```
    npm run production
    ```
2. All required code is in the static folder.

### Serve production code ###

1. In the root directory of the project, in console execute:

    ```
    npm run production
    ```
2. In the root directory of the project, in console execute:

    ```
    npm run serverdev
    ```
    to use [nodemon](https://github.com/remy/nodemon) - reloads the server when saving changes to its code

    or 

     ```
    npm run serverprod
    ```
    to fake a production server.