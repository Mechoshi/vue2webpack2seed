const webpack = require( "webpack" );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const ExtractTextPlugin = require( 'extract-text-webpack-plugin' );

module.exports = {
    entry: { app: './src/main.ts', },
    output: { filename: './build/app.js' },
    resolve: {
        extensions: [ '.ts', '.vue', '.js' ],
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                exclude: /node-modules/,
                options: {
                    esModule: true
                }
            },
            {
                test: /\.ts$/,
                exclude: /node-modules/,
                loader: 'vue-ts-loader'
            },
            {
                test: /\.html$/,
                exclude: /node-modules/,
                use: 'html-loader'
            },
            {
                test: /\.(woff|woff2|ttf|eot)$/,
                exclude: /node_modules/,
                loader: 'file-loader?name=build/assets/fonts/[name].[hash].[ext]'
            },
            {
                test: /\.(png|jpe?g|gif|svg|ico)$/,
                exclude: /node_modules/,
                loader: 'file-loader?name=build/assets/images/[name].[hash].[ext]'
            },
            {
                test: /\.scss$/,
                exclude: /src/,
                loader: ExtractTextPlugin.extract( { fallback: 'style-loader', use: [ 'css-loader?sourceMap', 'sass-loader', 'postcss-loader' ] } )
            }
        ],
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin( {
            name: [ 'app' ]
        } ),
        new HtmlWebpackPlugin( {
            template: './src/index.html'
        } ),
        new ExtractTextPlugin( './build/[name].css' )
    ]
}