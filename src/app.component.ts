import { Socket } from 'phoenix-socket';
import Vue from 'vue';
import Component from 'vue-class-component';
import { ChildComponent } from './components';
import * as PhoenixSocket from 'phoenix-socket';
import { SocketManager } from './services/socket.service';
import { Bus } from './services/bus.service';

@Component( {
  name: 'app',
  template: require( './app.component.html' ),
  components: {
    child: ChildComponent
  }
} )
export default class AppComponent extends Vue {

  public showChild: boolean = true;
  private message: string = 'Hello!';
  private socket: Socket;
  public dataForChild: Object = {
    msg: 'Hi, child!'
  }

  constructor() {
    super();
    this.socket = new PhoenixSocket.Socket( '/ws' );
    const ws: any = SocketManager.initSocket( '/ws2' );
    console.log( ws );
    console.log(process.env.NODE_ENV);
    console.log(process.env);
  }
  private onClick(): void {
    console.dir( this.socket );
    window.alert( this.message );
    Bus.$emit( 'BusEvent', { msg: 'Bus service is emiting a custom event' } );
  }


  public childEventHandler( payload: any ): void {
    alert( payload.msg );
  }



}