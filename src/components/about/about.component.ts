import Vue from 'vue';
import Component from 'vue-class-component';
import { HttpService } from './../../services/http.service';

/*interface axiosable {
  axios: any
}
*/

@Component( {
  template: require( './about.component.html' )
} )
export class AboutComponent extends Vue {

  private description: string = 'Vue.js project with TypeScript and Webpack 2 nad Phoenix framework';
  // public axios;

  constructor() {
    super();
    HttpService.get( 'https://jsonplaceholder.typicode.com/posts' )
      .then(( data: any ) => {
        console.log( data );
      } )
  }

  private getData(): void {

  }
}