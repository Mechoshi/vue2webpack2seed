import Vue from 'vue';
import Component from 'vue-class-component';
import { Bus } from './../../services/bus.service';

@Component( {
  name: 'child',
  template: require( './child.component.html' ),
  props: [
    'dataFromParent'
  ],
  watch: {
    watchHandler: this.watchHandler
  }
} )
export class ChildComponent extends Vue {

  /*Props defined in Component's decorator config object*/
  public dataFromParent: any;
  private msg: string = 'Hi,parent';

  /* Here a $emit functionality is tested - child custom event fired*/
  public emitToParent(): void {
    /*In develop mode it works with camelCAse event names... not in build... so USE lowercase CHILD EVENT NAMES*/
    this.$emit( 'childemits', { msg: this.msg } );
  }

  /*Watch handler. Defined in Component decorator's config obkect*/
  public watchHandler(): string {
    return this.msg + ' changed';
  }

  /* Lifecycle hook */
  public mounted(): void {
    if ( this.dataFromParent ) {
      console.log( this.dataFromParent.msg );
    }

    console.log( this.computedProperty );
  }

  /*Lifecycle hook - here we set event listener for the Bus event*/
  public created(): void {
    Bus.$on( 'BusEvent', this.busEventHandler );
  }

  /*Providing a named Bus event handler gives us the option to clean the event handler on destroyed lyfecycle hook*/
  private busEventHandler( payload: any ) {
    console.log( this );
    console.log( 'On BusEvent: ', payload.msg );
  }

  /*Dont put _ as name start - doesnt work that way!!!*/
  public cp: string = 'I am a computed property';


  /*TypeScript alternative to computed properties in JS mode - get and set methods*/
  set computedProperty( val: string ) {
    this.cp = val;
  }

  get computedProperty(): string {
    return this.cp;
  }

  public computedPropertyChanger( newValue: string ): void {
    this.computedProperty = newValue;
  }


  public watchTrigger(): void {
    this.msg += ' updated ';
  }

  public destroyed(): void {
    console.log( 'Destroyed from v-if' );
    /*Clean the event listener !!! */
    Bus.$off( 'BusEvent', this.busEventHandler );
  }

}