import Vue from "vue";
import AppComponent from "./app.component";
import "./../styles/main.scss";
import VueRouter from "vue-router";
import { AboutComponent } from "./components";
import axios from 'axios'
//import VueAxios from 'vue-axios'
 
//Vue.use( VueAxios, axios );

if(process.env.ENV === 'production') {
    console.log('Hi');
    Vue.config.devtools = false;
}

Vue.use( VueRouter );

const router = new VueRouter( {
    routes: [
        {
            path: '/about',
            component: AboutComponent
        }
    ]
} );

new Vue( {
    el: '#app',
    router: router,
    render: h => h( AppComponent )
} );

